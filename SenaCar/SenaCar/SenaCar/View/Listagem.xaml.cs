﻿using SenaCar.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SenaCar
{
    public class Servico
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public float Preco { get; set; }

        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco); }
        }
    }

    public partial class MainPage : ContentPage
    {
        public List<Servico> Servicos { get; set; }    

        public MainPage()
        {
            InitializeComponent();

            this.Servicos = new List<Servico>
            {
                new Servico { Marca = "Hyndai", Modelo = "Sonata", Preco = 89000 },
                new Servico { Marca = "Land Rover", Modelo = "Evoque", Preco = 142000 },
                new Servico { Marca = "Ford", Modelo = "Fusion", Preco = 79000},
                new Servico { Marca = "Ford", Modelo = "Edge", Preco = 99000 },
                new Servico { Marca = "Honda", Modelo = "Civic", Preco = 85000 }
            };

            this.BindingContext = this;

        }


        private void Listagem_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var servico = (Servico)e.Item;

            Navigation.PushAsync(new Descricao(servico));

        }
    }
}
